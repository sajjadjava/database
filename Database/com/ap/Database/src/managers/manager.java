package managers;
import com.google.gson.internal.LinkedTreeMap;
import modules.*;
import java.util.HashMap;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Map;

public class manager {
    public void manage(LinkedTreeMap action) throws ClassNotFoundException
    {
        if (action.get("Action").equals("create"))
        {
            try {
                create maker = new create();
                maker.create((LinkedTreeMap) action.get("Data"));
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
        else if (action.get("Action").equals("insert"))
        {
            try {
                insert adding = new insert();
                adding.insert((LinkedTreeMap) action.get("Data"));
            }
            catch(InputMismatchException E)
            {
                System.err.println(E.getMessage());
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
        else if (action.get("Action").equals("search"))
        {
            try {
                search browser = new search();
                browser.search((LinkedTreeMap) action.get("Data"));
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
        else if (action.get("Action").equals("update"))
        {
            try {
                update up_to_date = new update();
                up_to_date.update((LinkedTreeMap) action.get("Data"));
            }
            catch(InputMismatchException e)
            {
                System.err.println(e.getMessage());
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
        else if (action.get("Action").equals("delete"))
        {
            try {
                delete remove = new delete();
                remove.delete((LinkedTreeMap) action.get("Data"));
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
