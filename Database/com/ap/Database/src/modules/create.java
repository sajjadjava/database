package modules;
import com.google.gson.internal.LinkedTreeMap;

import java.io.*;
import java.util.HashMap;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.HashSet;

public class create {
    public void create(LinkedTreeMap Data) throws IOException
    {
        Files.createDirectory(Paths.get(String.format("com/ap/Database/src/Files/%s",Data.get("Name"))));
        Fields_structure(Data);
        createAllindex((LinkedTreeMap)Data.get("Fields"),(String)Data.get("Name"));
        create_DB((String)Data.get("Name"));
    }
    public void Fields_structure(LinkedTreeMap Data) throws IOException
    {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(String.format("com/ap/Database/src/Files/%s/%s",Data.get("Name"),"Fields_structure")));
        out.writeObject(Data.get("Fields"));
        out.close();
    }
    public void createAllindex(LinkedTreeMap Fields,String name) throws IOException
    {
        Set<String> fields = Fields.keySet();
        HashSet<String> units = new HashSet<>(fields);
        for (String field : units)
        {
            prepare_index(field,Fields,name);
        }
    }
    public void create_DB(String name) throws IOException
    {
        FileWriter out = new FileWriter(String.format("com/ap/Database/src/Files/%s/DB",name));
        out.write("");
        out.flush();
        out.close();
    }
    public void prepare_index(String field,LinkedTreeMap Fields,String name) throws FileNotFoundException,IOException
    {
        if (((LinkedTreeMap)Fields.get(field)).get("type").equals("string"))
        {
            LinkedHashMap<String,Integer> index = new LinkedHashMap<>();
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(String.format("com/ap/Database/src/Files/%s/%s",name,field)));
            out.writeObject(index);
            out.close();
        }
        else if (((LinkedTreeMap)Fields.get(field)).get("type").equals("int"))
        {
            LinkedHashMap<Integer,Integer> index = new LinkedHashMap<>();
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(String.format("com/ap/Database/src/Files/%s/%s",name,field)));
            out.writeObject(index);
            out.close();
        }
        else if (((LinkedTreeMap)Fields.get(field)).get("type").equals("long"))
        {
            LinkedHashMap<Long,Integer> index = new LinkedHashMap<>();
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(String.format("com/ap/Database/src/Files/%s/%s",name,field)));
            out.writeObject(index);
            out.close();
        }
        else if (((LinkedTreeMap)Fields.get(field)).get("type").equals("double"))
        {
            LinkedHashMap<Double,Integer> index = new LinkedHashMap<>();
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(String.format("com/ap/Database/src/Files/%s/%s",name,field)));
            out.writeObject(index);
            out.close();
        }
        else  if (((LinkedTreeMap)Fields.get(field)).get("type").equals("boolean"))
        {
            LinkedHashMap<Boolean,Integer> index = new LinkedHashMap<>();
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(String.format("com/ap/Database/src/Files/%s/%s",name,field)));
            out.writeObject(index);
            out.close();
        }
    }
}
