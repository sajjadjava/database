package modules;

import com.google.gson.internal.LinkedTreeMap;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

public class delete {
    public void delete(LinkedTreeMap Data) throws IOException,ClassNotFoundException
    {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(String.format("com/ap/Database/src/Files/%s/Fields_structure",Data.get("Table"))));
        LinkedHashMap fields = (LinkedHashMap) in.readObject();
        in.close();
        ArrayList delete_list = list_maker((String)Data.get("Table"),(String)Data.get("primary_key"),(ArrayList)Data.get("Info"),fields);
        int size = size_of_rows(fields);
        delete_results((String)Data.get("Table"),delete_list,size,fields);
    }
    public ArrayList<Integer> list_maker(String table, String primary_key, ArrayList Info, LinkedHashMap fields) throws IOException,ClassNotFoundException
    {
        ArrayList<Integer> delete_list = new ArrayList<>();
        ObjectInputStream input = new ObjectInputStream(new FileInputStream(String.format("com/ap/Database/src/Files/%s/%s",table,primary_key)));
        LinkedHashMap delete_elements = (LinkedHashMap) input.readObject();
        input.close();
        if (((LinkedHashMap)fields.get(primary_key)).get("type").equals("int"))
        {
            for (int i=0;i<Info.size();i++)
            {
                Integer key = (Integer)(int)(double)(Double)Info.get(i);
                delete_list.add((Integer)delete_elements.get(key));
            }
        }
        else if (((LinkedHashMap)fields.get(primary_key)).get("type").equals("long"))
        {
            for (int i=0;i<Info.size();i++)
            {
                Long key = (Long)(long)(int)(double)(Double)Info.get(i);
                delete_list.add((Integer)delete_elements.get(key));
            }
        }
        else
        {
            for (int i=0;i<Info.size();i++)
            {
                delete_list.add((Integer)delete_elements.get(Info.get(i)));
            }
        }
        return delete_list;
    }
    public int size_of_rows(LinkedHashMap fields) throws IOException
    {
        LinkedHashMap<String,Object> artifact = new LinkedHashMap<>();
        Set<String> parts = fields.keySet();
        HashSet<String> parts_2 = new HashSet<>(parts);
        for(String part : parts_2)
        {
            if(((LinkedHashMap)fields.get(part)).get("type").equals("string"))
            {
                StringBuilder temporary = new StringBuilder();
                int string = (Integer)(int)(double)((LinkedHashMap)fields.get(part)).get("size");
                for(int i=0;i<string;i++)
                    temporary.append("s");
                artifact.put(part,temporary.toString());
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("int"))
            {
                Double temporary = 3.0;
                artifact.put(part,temporary);
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("long"))
            {
                Double temporary = 1.0;
                artifact.put(part,temporary);
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("double"))
            {
                Double temporary = 2.0;
                artifact.put(part,temporary);
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("boolean"))
            {
                Boolean temporary = true;
                artifact.put(part,temporary);
            }
        }
        ByteArrayOutputStream artifact_array = new ByteArrayOutputStream();
        ObjectOutputStream artifact_maker = new ObjectOutputStream(artifact_array);
        artifact_maker.writeObject(artifact);
        byte[] for_artifact = artifact_array.toByteArray();
        artifact_maker.close();
        return for_artifact.length;
    }
    public void delete_results(String table,ArrayList<Integer> delete_list,int size,LinkedHashMap fields) throws FileNotFoundException,IOException,ClassNotFoundException
    {
        RandomAccessFile in = new RandomAccessFile(String.format("com/ap/Database/src/Files/%s/DB",table),"rw");
        for (Integer place : delete_list)
        {
            in.seek((place-1)*size);
            byte[] bytes = new byte[size];
            in.read(bytes,0,bytes.length);
            ByteArrayInputStream inward = new ByteArrayInputStream(bytes);
            ObjectInputStream input = new ObjectInputStream(inward);
            LinkedHashMap row = (LinkedHashMap) input.readObject();
            index_implement(row,table,fields);
            input.close();
            in.seek((place-1)*size);
            byte[] Bytes = new byte[size];
            in.write(Bytes,0,bytes.length);
        }
        in.close();
    }
    public void index_implement(LinkedHashMap row,String file,LinkedHashMap fields) throws FileNotFoundException,IOException,ClassNotFoundException
    {
        Set<String> row_items = row.keySet();
        HashSet<String> row_units = new HashSet<>(row_items);
        for (String unit : row_units)
        {
            ObjectInputStream in =new ObjectInputStream(new FileInputStream(String.format("com/ap/Database/src/Files/%s/%s",file,unit)));
            LinkedHashMap index = (LinkedHashMap) in.readObject();
            in.close();
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(String.format("com/ap/Database/src/Files/%s/%s",file,unit)));
            if (((LinkedHashMap)fields.get(unit)).get("type").equals("int"))
            {
                index.remove((Integer)(int)(double)row.get(unit));
            }
            else if(((LinkedHashMap)fields.get(unit)).get("type").equals("long"))
            {
                index.remove((Long)(long)(int)(double)row.get(unit));
            }
            else
            {
                index.remove(row.get(unit));
            }
            out.writeObject(index);
            out.close();
        }
    }
}
