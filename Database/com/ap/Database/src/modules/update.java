package modules;

import com.google.gson.internal.LinkedTreeMap;

import java.io.*;
import java.util.*;

public class update {
    public void update(LinkedTreeMap Data) throws IOException,ClassNotFoundException
    {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(String.format("com/ap/Database/src/Files/%s/Fields_structure",Data.get("Table"))));
        LinkedHashMap field_structure = (LinkedHashMap) in.readObject();
        in.close();
        int size = size_of_rows(field_structure);
        ArrayList<Object> Info = new ArrayList<>();
        Info = (ArrayList<Object>) Data.get("Info");
        for (int i=0;i<Info.size();i++)
        {
            if (validation((LinkedTreeMap)Info.get(i),field_structure))
            {
                int location = find_location((String)Data.get("Table"),(LinkedTreeMap)((LinkedTreeMap)Info.get(i)).get("primary_key"),field_structure);
                send_to_file((LinkedTreeMap)Info.get(i),(String)Data.get("Table"),size,field_structure,location);
            }
            else
            {
                throw new InputMismatchException("your input is incorrect");
            }
        }
    }
    public boolean validation(LinkedTreeMap row,LinkedHashMap field_structure)
    {
        if (field_structure.size()+1 != row.size())
        {
            return false;
        }
        Set<String> fields = field_structure.keySet();
        HashSet<String> Fields = new HashSet<>(fields);
        for(String field : Fields)
        {
            if (!(row.containsKey(field)))
            {
                return false;
            }
            if((((LinkedHashMap)field_structure.get(field)).get("type")).equals("string"))
            {
                double size = (Double)((LinkedHashMap)field_structure.get(field)).get("size");
                if (!(row.get(field) instanceof String))
                    return false;
                if (((String)(row.get(field))).length() > size)
                    return false;
            }
            else if((((LinkedHashMap)field_structure.get(field)).get("type")).equals("boolean"))
            {
                if(!(row.get(field) instanceof Boolean))
                    return false;
            }
            else if ((((LinkedHashMap)field_structure.get(field)).get("type")).equals("int"))
            {
                if(((Double)row.get(field))-((Integer)(int)(double)row.get(field)) != 0)
                    return false;
            }
            else if ((((LinkedHashMap)field_structure.get(field)).get("type")).equals("long"))
            {
                if ((Double)row.get(field)-((Long)(long)(int)(double)row.get(field)) != 0)
                    return false;
            }
            else if ((((LinkedHashMap)field_structure.get(field)).get("type")).equals("double"))
            {
                if(!(row.get(field) instanceof Double))
                    return false;
            }
        }
        Set<String> primary_key_1 = ((LinkedTreeMap)row.get("primary_key")).keySet();
        HashSet<String> primary_key_2 = new HashSet<>(primary_key_1);
        for (String key : primary_key_2)
        {
            if (!(field_structure.containsKey(key)))
            {
                return false;
            }
            if((((LinkedHashMap)field_structure.get(key)).get("type")).equals("string"))
            {
                double size = (Double)((LinkedHashMap)field_structure.get(key)).get("size");
                if (!(((LinkedTreeMap)row.get("primary_key")).get(key) instanceof String))
                    return false;
                if (((String)((LinkedTreeMap)row.get("primary_key")).get(key)).length() > size)
                    return false;
            }
            else if((((LinkedHashMap)field_structure.get(key)).get("type")).equals("boolean"))
            {
                if(!(((LinkedTreeMap)row.get("primary_key")).get(key) instanceof Boolean))
                    return false;
            }
            else if ((((LinkedHashMap)field_structure.get(key)).get("type")).equals("int"))
            {
                if(((Double)((LinkedTreeMap)row.get("primary_key")).get(key))-((Integer)(int)(double)((LinkedTreeMap)row.get("primary_key")).get(key)) != 0)
                    return false;
            }
            else if ((((LinkedHashMap)field_structure.get(key)).get("type")).equals("long"))
            {
                if (((Double)((LinkedTreeMap)row.get("primary_key")).get(key))-((Long)(long)(int)(double)((LinkedTreeMap)row.get("primary_key")).get(key)) != 0)
                    return false;
            }
            else if ((((LinkedHashMap)field_structure.get(key)).get("type")).equals("double"))
            {
                if(!(((LinkedTreeMap)row.get("primary_key")).get(key) instanceof Double))
                    return false;
            }
        }
        return true;
    }
    public int size_of_rows(LinkedHashMap fields) throws IOException
    {
        LinkedHashMap<String,Object> artifact = new LinkedHashMap<>();
        Set<String> parts = fields.keySet();
        HashSet<String> parts_2 = new HashSet<>(parts);
        for(String part : parts_2)
        {
            if(((LinkedHashMap)fields.get(part)).get("type").equals("string"))
            {
                StringBuilder temporary = new StringBuilder();
                int string = (Integer)(int)(double)((LinkedHashMap)fields.get(part)).get("size");
                for(int i=0;i<string;i++)
                    temporary.append("s");
                artifact.put(part,temporary.toString());
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("int"))
            {
                Double temporary = 3.0;
                artifact.put(part,temporary);
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("long"))
            {
                Double temporary = 1.0;
                artifact.put(part,temporary);
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("double"))
            {
                Double temporary = 2.0;
                artifact.put(part,temporary);
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("boolean"))
            {
                Boolean temporary = true;
                artifact.put(part,temporary);
            }
        }
        ByteArrayOutputStream artifact_array = new ByteArrayOutputStream();
        ObjectOutputStream artifact_maker = new ObjectOutputStream(artifact_array);
        artifact_maker.writeObject(artifact);
        byte[] for_artifact = artifact_array.toByteArray();
        artifact_maker.close();
        return for_artifact.length;
    }
    public int find_location(String table,LinkedTreeMap primary_key,LinkedHashMap fields) throws IOException,ClassNotFoundException {
        Set<String> key_1 = primary_key.keySet();
        List<String> key_2 = new ArrayList<>(key_1);
        String key = key_2.get(0);
        ObjectInputStream input = new ObjectInputStream(new FileInputStream(String.format("com/ap/Database/src/Files/%s/%s", table, key)));
        LinkedHashMap locations = (LinkedHashMap) input.readObject();
        input.close();
        if (((LinkedHashMap) fields.get(key)).get("type").equals("int")) {
            return (int) locations.get((Integer)(int)(double) (primary_key.get(key)));
        } else if (((LinkedHashMap) fields.get(key)).get("type").equals("long")) {
            return (int) (locations.get((Long)(long)(int)(double) (primary_key.get(key))));
        } else {
            return (int) (locations.get(primary_key.get(key)));
        }
    }
    public void send_to_file(LinkedTreeMap row,String file,int size,LinkedHashMap fields,int location) throws IOException,ClassNotFoundException
    {
        long begin_place = (long)(location-1)*size;
        LinkedTreeMap<String,Object> section_0 = convert(row);
        RandomAccessFile put = new RandomAccessFile(String.format("com/ap/Database/src/Files/%s/DB",file),"rw");
        put.seek(begin_place);
        byte[] trail = new byte[(int)size];
        put.read(trail,0,trail.length);
        ByteArrayInputStream coming = new ByteArrayInputStream(trail);
        ObjectInputStream coming_in = new ObjectInputStream(coming);
        LinkedHashMap section = (LinkedHashMap) coming_in.readObject();
        index_implement(section,section_0,file,(int)(begin_place/size)+1,fields);
        ByteArrayOutputStream bin = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bin);
        out.writeObject(section_0);
        out.close();
        byte[] bytes = bin.toByteArray();
        byte[] tail = new byte[(int)size];
        for(int i=0;i<bytes.length;i++)
        {
            tail[i]=bytes[i];
        }
        put.seek(begin_place);
        put.write(tail,0,tail.length);
        put.close();
    }
    public void index_implement(LinkedHashMap section,LinkedTreeMap section_0,String file,int place,LinkedHashMap fields) throws FileNotFoundException,IOException,ClassNotFoundException
    {
        Set<String> section_items = section_0.keySet();
        HashSet<String> section_units = new HashSet<>(section_items);
        for (String unit : section_units)
        {
            ObjectInputStream in =new ObjectInputStream(new FileInputStream(String.format("com/ap/Database/src/Files/%s/%s",file,unit)));
            LinkedHashMap index = (LinkedHashMap) in.readObject();
            in.close();
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(String.format("com/ap/Database/src/Files/%s/%s",file,unit)));
            if (((LinkedHashMap)fields.get(unit)).get("type").equals("int"))
            {
                index.remove((Integer)(int)(double)section.get(unit));
                index.put((Integer)(int)(double)section_0.get(unit),place);
            }
            else if(((LinkedHashMap)fields.get(unit)).get("type").equals("long"))
            {
                index.remove((Long)(long)(int)(double)section.get(unit));
                index.put((Long)(long)(int)(double)section_0.get(unit),place);
            }
            else
            {
                index.remove(section.get(unit));
                index.put(section_0.get(unit),place);
            }
            out.writeObject(index);
            out.close();
        }
    }
    public LinkedTreeMap convert(LinkedTreeMap row)
    {
        LinkedTreeMap section = new LinkedTreeMap();
        Set<String> key_1 = row.keySet();
        HashSet<String> key_2 = new HashSet<>(key_1);
        for (String key : key_2)
        {
            if (!(key.equals("primary_key")))
            {
                section.put(key,row.get(key));
            }
        }
        return section;
    }
}
