package modules;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

public class search {
    public void search(LinkedTreeMap Data)throws IOException,ClassNotFoundException
    {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(String.format("com/ap/Database/src/Files/%s/Fields_structure",Data.get("Table"))));
        LinkedHashMap fields = (LinkedHashMap) in.readObject();
        in.close();
        ArrayList<Integer> search_list = list_maker((String)Data.get("Table"),(String)Data.get("primary_key"),(ArrayList)Data.get("Info"),fields);
        int size = size_of_rows(fields);
        ArrayList<Object> rows = new ArrayList<>();
        rows = search_results((String)Data.get("Table"),search_list,size,fields);
        Gson gson = new Gson();
        String json = gson.toJson(rows);
        System.out.println(json);
    }
    public ArrayList<Integer> list_maker(String table,String primary_key,ArrayList Info,LinkedHashMap fields) throws IOException,ClassNotFoundException
    {
        ArrayList<Integer> search_list = new ArrayList<>();
        ObjectInputStream input = new ObjectInputStream(new FileInputStream(String.format("com/ap/Database/src/Files/%s/%s",table,primary_key)));
        LinkedHashMap search_elements = (LinkedHashMap) input.readObject();
        input.close();
        if (((LinkedHashMap)fields.get(primary_key)).get("type").equals("int"))
        {
            for (int i=0;i<Info.size();i++)
            {
                Integer key = (Integer)(int)(double)(Double)Info.get(i);
                search_list.add((Integer)search_elements.get(key));
            }
        }
        else if (((LinkedHashMap)fields.get(primary_key)).get("type").equals("long"))
        {
            for (int i=0;i<Info.size();i++)
            {
                Long key = (Long)(long)(int)(double)(Double)Info.get(i);
                search_list.add((Integer)search_elements.get(key));
            }
        }
        else
        {
            for (int i=0;i<Info.size();i++)
            {
                search_list.add((Integer)search_elements.get(Info.get(i)));
            }
        }
        return search_list;
    }
    public ArrayList<Object> search_results(String table,ArrayList<Integer> search_list,int size,LinkedHashMap fields) throws FileNotFoundException,IOException,ClassNotFoundException
    {
        ArrayList<Object> rows = new ArrayList<>();
        RandomAccessFile in = new RandomAccessFile(String.format("com/ap/Database/src/Files/%s/DB",table),"rw");
        for (Integer place : search_list)
        {
            in.seek((place-1)*size);
            byte[] bytes = new byte[size];
            in.read(bytes,0,bytes.length);
            ByteArrayInputStream Bytes = new ByteArrayInputStream(bytes);
            ObjectInputStream input = new ObjectInputStream(Bytes);
            LinkedHashMap row = (LinkedHashMap) input.readObject();
            rows.add(prepare_row(row,fields));
        }
        in.close();
        return rows;
    }
    public int size_of_rows(LinkedHashMap fields) throws IOException
    {
        LinkedHashMap<String,Object> artifact = new LinkedHashMap<>();
        Set<String> parts = fields.keySet();
        HashSet<String> parts_2 = new HashSet<>(parts);
        for(String part : parts_2)
        {
            if(((LinkedHashMap)fields.get(part)).get("type").equals("string"))
            {
                StringBuilder temporary = new StringBuilder();
                int string = (Integer)(int)(double)((LinkedHashMap)fields.get(part)).get("size");
                for(int i=0;i<string;i++)
                    temporary.append("s");
                artifact.put(part,temporary.toString());
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("int"))
            {
                Double temporary = 3.0;
                artifact.put(part,temporary);
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("long"))
            {
                Double temporary = 1.0;
                artifact.put(part,temporary);
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("double"))
            {
                Double temporary = 2.0;
                artifact.put(part,temporary);
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("boolean"))
            {
                Boolean temporary = true;
                artifact.put(part,temporary);
            }
        }
        ByteArrayOutputStream artifact_array = new ByteArrayOutputStream();
        ObjectOutputStream artifact_maker = new ObjectOutputStream(artifact_array);
        artifact_maker.writeObject(artifact);
        byte[] for_artifact = artifact_array.toByteArray();
        artifact_maker.close();
        return for_artifact.length;
    }
    public LinkedHashMap prepare_row(LinkedHashMap row,LinkedHashMap fields)
    {
        Set<String> fields_sections = fields.keySet();
        HashSet<String> fields_units = new HashSet<>(fields_sections);
        for (String unit : fields_units)
        {
            if (((LinkedHashMap)fields.get(unit)).get("type").equals("int"))
            {
                Double section = (Double)row.get(unit);
                row.put(unit,(Integer)(int)(double)section);
            }
            else if (((LinkedHashMap)fields.get(unit)).get("type").equals("long"))
            {
                Double section = (Double)row.get(unit);
                row.put(unit,(Long)(long)(int)(double)section);
            }
        }
        return row;
    }
}
