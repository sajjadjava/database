package modules;
import com.google.gson.internal.LinkedTreeMap;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.*;


public class insert
{
    public void insert(LinkedTreeMap Data) throws IOException,ClassNotFoundException
    {
        if(!(All_validations((List<LinkedTreeMap<String,Object>>)Data.get("Info"),(String)Data.get("Table"))))
        {
            throw new InputMismatchException("your input is incorrect");
        }
        All_sends_to_file(Data);

    }
    public boolean All_validations(List<LinkedTreeMap<String,Object>> Info,String table) throws IOException,ClassNotFoundException
    {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(String.format("com/ap/Database/src/Files/%s/Fields_structure",table)));
        LinkedHashMap<String,Object> structure = (LinkedHashMap<String,Object>) in.readObject();
        in.close();
        for (int j=0;j<Info.size();j++)
        {
            if(!(validation(structure,Info.get(j))))
                return false;
        }
        return true;
    }
    public boolean validation(LinkedHashMap<String,Object> field_structure,LinkedTreeMap<String,Object> row)
    {
        if (field_structure.size() != row.size())
        {
            return false;
        }
        Set<String> fields = field_structure.keySet();
        HashSet<String> Fields = new HashSet<>(fields);
        for(String field : Fields)
        {
            if (!(row.containsKey(field)))
            {
                return false;
            }
            if((((LinkedHashMap)field_structure.get(field)).get("type")).equals("string"))
            {
                double size = (Double)((LinkedHashMap)field_structure.get(field)).get("size");
                if (!(row.get(field) instanceof String))
                    return false;
                if (((String)(row.get(field))).length() > size)
                    return false;
            }
            else if((((LinkedHashMap)field_structure.get(field)).get("type")).equals("boolean"))
            {
                if(!(row.get(field) instanceof Boolean))
                    return false;
            }
            else if ((((LinkedHashMap)field_structure.get(field)).get("type")).equals("int"))
            {
                if(((Double)row.get(field))-((Integer)(int)(double)row.get(field)) != 0)
                    return false;
            }
            else if ((((LinkedHashMap)field_structure.get(field)).get("type")).equals("long"))
            {
                if ((Double)row.get(field)-((Long)(long)(int)(double)row.get(field)) != 0)
                    return false;
            }
            else if ((((LinkedHashMap)field_structure.get(field)).get("type")).equals("double"))
            {
                if(!(row.get(field) instanceof Double))
                    return false;
            }
        }
        return true;
    }
    public void All_sends_to_file(LinkedTreeMap Data) throws FileNotFoundException,IOException,ClassNotFoundException
    {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(String.format("com/ap/Database/src/Files/%s/Fields_structure",Data.get("Table"))));
        LinkedHashMap fields = (LinkedHashMap)in.readObject();
        in.close();
        long size = size_of_rows(fields);
        List<LinkedTreeMap> Info = new ArrayList<LinkedTreeMap>();
        Info =(ArrayList<LinkedTreeMap>) Data.get("Info");
        for(LinkedTreeMap section : Info)
        {
            send_to_file(section,(String)Data.get("Table"),size,fields);
        }
    }
    public void send_to_file(LinkedTreeMap section,String file,long size,LinkedHashMap fields) throws IOException,ClassNotFoundException
    {
        long begin_place = search_empty_space(file,size);
        index_implement(section,file,(int)(begin_place/size)+1,fields);
        ByteArrayOutputStream bin = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bin);
        out.writeObject(section);
        out.close();
        byte[] bytes = bin.toByteArray();
        byte[] tail = new byte[(int)size];
        for(int i=0;i<bytes.length;i++)
        {
            tail[i]=bytes[i];
        }
        RandomAccessFile put = new RandomAccessFile(String.format("com/ap/Database/src/Files/%s/DB",file),"rw");
        put.seek(begin_place);
        put.write(tail,0,tail.length);
        put.close();
    }
    public int size_of_rows(LinkedHashMap fields) throws IOException
    {
        LinkedHashMap<String,Object> artifact = new LinkedHashMap<>();
        Set<String> parts = fields.keySet();
        HashSet<String> parts_2 = new HashSet<>(parts);
        for(String part : parts_2)
        {
            if(((LinkedHashMap)fields.get(part)).get("type").equals("string"))
            {
                StringBuilder temporary = new StringBuilder();
                int string = (Integer)(int)(double)((LinkedHashMap)fields.get(part)).get("size");
                for(int i=0;i<string;i++)
                    temporary.append("s");
                artifact.put(part,temporary.toString());
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("int"))
            {
                Double temporary = 3.0;
                artifact.put(part,temporary);
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("long"))
            {
                Double temporary = 1.0;
                artifact.put(part,temporary);
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("double"))
            {
                Double temporary = 2.0;
                artifact.put(part,temporary);
            }
            else if (((LinkedHashMap)fields.get(part)).get("type").equals("boolean"))
            {
                Boolean temporary = true;
                artifact.put(part,temporary);
            }
        }
        ByteArrayOutputStream artifact_array = new ByteArrayOutputStream();
        ObjectOutputStream artifact_maker = new ObjectOutputStream(artifact_array);
        artifact_maker.writeObject(artifact);
        byte[] for_artifact = artifact_array.toByteArray();
        artifact_maker.close();
        return for_artifact.length;
    }
    public long search_empty_space(String file,long size) throws IOException
    {
        Path DB = Paths.get(String.format("com/ap/Database/src/Files/%s/DB",file));
        long volume = Files.size(DB);
        RandomAccessFile pointer = new RandomAccessFile(DB.toFile(),"rw");
        if (volume == 0)
            return 0;
        for (int i=0;i<(volume/size);i++)
        {
            byte[] bytes = new byte[1];
            pointer.seek(i*size);
            pointer.read(bytes,0,1);
            byte[] temporary = new byte[1];
            if (bytes[0] == temporary[0] || bytes[0] == -1)
                return (long)i*size;
        }
        pointer.close();
        return Files.size(DB);
    }
    public void index_implement(LinkedTreeMap section,String file,int place,LinkedHashMap fields) throws FileNotFoundException,IOException,ClassNotFoundException
    {
        Set<String> section_items = section.keySet();
        HashSet<String> section_units = new HashSet<>(section_items);
        for (String unit : section_units)
        {
            ObjectInputStream in =new ObjectInputStream(new FileInputStream(String.format("com/ap/Database/src/Files/%s/%s",file,unit)));
            LinkedHashMap index = (LinkedHashMap) in.readObject();
            in.close();
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(String.format("com/ap/Database/src/Files/%s/%s",file,unit)));
            if (((LinkedHashMap)fields.get(unit)).get("type").equals("int"))
            {
                index.put((Integer)(int)(double)section.get(unit),place);
            }
            else if(((LinkedHashMap)fields.get(unit)).get("type").equals("long"))
            {
                index.put((Long)(long)(int)(double)section.get(unit),place);
            }
            else
            {
                index.put(section.get(unit),place);
            }
            out.writeObject(index);
            out.close();
        }
    }
}
