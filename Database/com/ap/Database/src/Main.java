import java.io.*;
import java.util.*;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import managers.*;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException,IOException
    {
        Scanner in = new Scanner(System.in);
        System.out.print("enter the address of your entrance jsonfile: ");
        Path entrance = Paths.get(in.next());
        System.out.println();
        Gson gson = new Gson();
        LinkedTreeMap input = gson.fromJson((estekhraj(entrance)).toString(), LinkedTreeMap.class);
        ArrayList<Object> commands =(ArrayList<Object>) input.get("commands");
        manager engineer = new manager();
        for (int i=0;i<commands.size();i++)
        {
            engineer.manage((LinkedTreeMap)commands.get(i));
        }
    }
    public static StringBuilder estekhraj(Path in) throws IOException
    {
        StringBuilder file = new StringBuilder();
        Scanner input = new Scanner(in);
        while (input.hasNext())
        {
            file.append(input.nextLine());
        }
        return file;
    }
}
