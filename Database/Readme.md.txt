yo must give a json Object to program.
this Object must have one ArrayList(the name of ArrayList is commands) which has some actions.
----------------------------------------------------------------------------------------------------------
create Object
{
  "Action": "create",
  "Data": {
    "Name": "roads",
    "Fields": {
      "Name": {
        "type": "string",
        "size": 90
      },
      "length":{
        "type": "long"
      },
      "ff": {
        "type": "boolean"
      }
    }
  }
} 
in this Object, Name in Data is your table name and Fields in Data is your columns qualities.
insert Object
----------------------------------------------------------------------------------------------------------
{
  "Action": "insert",
  "Data": {
    "Table": "roads",
    "Info": [
      {
        "Name": "azadi",
        "length": 20,
        "ff": true
      },
      {
        "Name": "zahra",
        "length": 10,
        "ff": true
      }
    ]
  }
}
in this Object, Table in Data is name of the insertable table and Info is List of insertable rows.
----------------------------------------------------------------------------------------------------------
search Object
{
  "Action": "search",
  "Data": {
    "Table": "roads",
    "primary_key": "length",
    "Info": [
      10,
      20
    ]
  }
}
in this Object, Table in Data is name of the searchable table and primary_key specifies certain column in table that we search based on that column and the Info is List of some values in that column and program search the rows that have this values in that column.
----------------------------------------------------------------------------------------------------------
update Object
{
  "Action": "update",
  "Data": {
    "Table": "roads",
    "Info": [
      {
        "primary_key": {"Name": "azadi"},
        "Name": "sasan",
        "length": 2,
        "ff": false
      },
      {
        "primary_key": {"Name": "zahra"},
        "Name": "akbar",
        "length": 19,
        "ff": false
      }
    ]
  }
}
in this Object, Table in Data is name of the updateable table and Info is List of updateable rows of that table. in each elament of Info we have primary_key Object that specifies certain row in table, in primary_key you determine the value of certain column and whith that you detemine the certain row to program and in below of primary_key you write the updateable data but note that you must write the value of whole columns in table for updateable row
-----------------------------------------------------------------------------------------------------------
delete Object
{
  "Action": "delete",
  "Data": {
    "Table": "roads",
    "primary_key": "length",
    "Info": [
      40
    ]
  }
}
in this Object, Table in Data is name of the updateable table and primary_key specifies certain column in table that we delete rows based on the values of that column and the Info is List of some values in that column and program delete the rows that have these values in that column.
-----------------------------------------------------------------------------------------------------------
we have one full example in below
{
  "commands": [
    {
      "Action": "create",
      "Data": {
        "Name": "roads",
        "Fields": {
          "Name": {
            "type": "string",
            "size": 90
          },
          "length":{
            "type": "long"
          },
          "ff": {
            "type": "boolean"
          }
        }
      }
    },
    {
      "Action": "create",
      "Data": {
        "Name": "ali",
        "Fields": {
          "Name": {
            "type": "int"
          },
          "age": {
            "type": "int"
          }
        }
      }
    },
    {
      "Action": "insert",
      "Data": {
        "Table": "roads",
        "Info": [
          {
            "Name": "azadi",
            "length": 20,
            "ff": true
          },
          {
            "Name": "zahra",
            "length": 10,
            "ff": true
          },
          {
            "Name": "maziar",
            "length": 40,
            "ff": false
          }
        ]
      }
    },
    {
      "Action": "insert",
      "Data": {
        "Table": "ali",
        "Info": [
          {
            "Name": 10,
            "age": 20
          },
          {
            "Name": 24,
            "age": 3
          }
        ]
      }
    },
    {
      "Action": "update",
      "Data": {
        "Table": "roads",
        "Info": [
          {
            "primary_key": {"Name": "azadi"},
            "Name": "sasan",
            "length": 2,
            "ff": false
          },
          {
            "primary_key": {"Name": "zahra"},
            "Name": "akbar",
            "length": 19,
            "ff": false
          }
        ]
      }
    },
    {
      "Action": "search",
      "Data": {
        "Table": "roads",
        "primary_key": "length",
        "Info": [
          40,
          19,
          2
        ]
      }
    },
    {
      "Action": "delete",
      "Data": {
        "Table": "roads",
        "primary_key": "length",
        "Info": [
          40
        ]
      }
    },
    {
      "Action": "search",
      "Data": {
        "Table": "roads",
        "primary_key": "length",
        "Info": [
          19,
          2
        ]
      }
    }
  ]
}
